﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace MobileNetworkTrainingApp
{
    public class HomeController : Controller
    {
        private static Guid token = Guid.NewGuid();

        [Route("~/api/[controller]")]
        [HttpGet]
        public IActionResult GetToken()
        {
            return Ok(token);
        }

        [Route("~/api/[controller]")]
        [HttpPost]
        public IActionResult GetInfo()
        {
            if (Request.Headers["token"].ToString() == token.ToString())
            {
                token = Guid.NewGuid();
                return Ok("Working");
            }

            return BadRequest();
        }
    }
}